import React, {PureComponent} from 'react';
import {Alert} from 'react-native';
import {RNCamera} from 'react-native-camera';
import Icon from 'react-native-vector-icons/FontAwesome';

export class Camera extends PureComponent {
  takePicture = async () => {
    if (this.camera && !this.props.takingPic) {
      let options = {
        quality: 0.85,
        fixOrientation: true,
        forceUpOrientation: true,
      };
      this.props.takePhotoHandler();

      try {
        const data = await this.camera.takePictureAsync(options);
        // Alert.alert('Success', JSON.stringify(data));
        this.props.onPicture(data);
      } catch (err) {
        Alert.alert('Error', 'Failed to take picture: ' + (err.message || err));
        return;
      } finally {
        this.props.takePhotoHandler();
      }
    }
  };

  render() {
    return (
      <>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          captureAudio={false}
          style={{flex: 1}}
          type={RNCamera.Constants.Type.back}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        />

        <Icon
          style={{
            position: 'absolute',
            bottom: 0,
            left: '45%',
            paddingBottom: 20,
          }}
          size={50}
          name="camera-retro"
          onPress={this.takePicture}
          color="#fff"
        />
      </>
    );
  }
}

export default Camera;
