import React, {Component} from 'react';
import {
  SafeAreaView,
  Alert,
  TouchableHighlight,
  Image,
  Button,
  PermissionsAndroid,
  Platform,
} from 'react-native';
import Camera from './component/Camera';
import Icon from 'react-native-vector-icons/FontAwesome';
import CameraRoll from '@react-native-community/cameraroll';
import PushNotification from 'react-native-push-notification';

export class App extends Component {
  state = {
    img: null,
    saving: false,
    takingPic: false,
  };
  onPicture = ({uri}) => {
    this.setState({
      img: uri,
    });
  };
  onBackToCamera = () => {
    this.setState({
      img: null,
    });
  };

  getPermissionAndroid = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Image Download Permission',
          message: 'Your permission is required to save images to your device',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        return true;
      }

      Alert.alert(
        'Save remote Image',
        'Grant Me Permission to save Image',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
    } catch (err) {
      Alert.alert(
        'Save remote Image',
        'Failed to save Image: ' + err.message,
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
    }
  };

  saveImage = async () => {
    if (Platform.OS === 'android') {
      const granted = await this.getPermissionAndroid();
      if (!granted) {
        return;
      }
    }

    CameraRoll.save(this.state.img, 'photo');
    // Alert.alert(
    //   'Save remote Image',
    //   'Image Saved Successfully',
    //   [{text: 'OK', onPress: () => this.setState({saving: true})}],
    //   {cancelable: false},
    // );

    PushNotification.createChannel(
      {
        channelId: '123',
        channelName: 'notification',
        soundName: 'default',
        importance: 4,
        vibrate: true,
      },
      created => console.log(`createChannel returned '${created}'`),
    );

    PushNotification.localNotification({
      channelId: '123',
      message: 'Image Saved',
    });
    this.onBackToCamera();
  };
  takePhotoHandler = () => {
    this.setState({takingPic: !this.state.takingPic});
  };

  render() {
    console.log('App started....');
    return (
      <SafeAreaView style={{flex: 1}}>
        {this.state.img ? (
          <TouchableHighlight style={{flex: 1}} onPress={this.onBackToCamera}>
            <>
              <Image style={{flex: 1}} source={{uri: this.state.img}} />
              <Icon
                style={{
                  position: 'absolute',
                  bottom: 0,
                  left: '45%',
                  paddingBottom: 20,
                }}
                size={50}
                name="save"
                onPress={this.saveImage}
                color="#fff"
              />
            </>
          </TouchableHighlight>
        ) : (
          <Camera
            takingPic={this.state.takingPic}
            name="anirudh"
            onPicture={this.onPicture}
            takePhotoHandler={this.takePhotoHandler}
          />
        )}
      </SafeAreaView>
    );
  }
}

export default App;
